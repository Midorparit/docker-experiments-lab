# Required

* Hypervisor like [VirtualBox](https://www.virtualbox.org/), Hyper-V or similar. Note: Hyper-V can't work beside VirtualBox
* [Vagrant](https://www.vagrantup.com/)

# Configuration
The file `./servers.json` contains the definitions of the virtual machines. To add another virtual machine, just add one new definition to the array.

# Connectivity

## SSH

Once the virtual machines are running, the typical SSH port 22 of each virtual machine is mapped to the corresponding ssh_port defined in the `./servers.json`. So to log in via SSH one can use the following commands:

    $ ssh vagrant@localhost -p <ssh_port_of_vm_n>
or

    $ ssh vagrant@<ip_addr_of_physical_host> -p <ssh_port_of_vm_n>

The virtual machines are connected to a [internal network](https://nocksoft.de/tutorials/virtualbox-netzwerkkonfiguration/) if the hypervisor is VirtualBox or to a host-only network as fallback if another hypervisor like Hyper-V is used. Inside that network the other machines can be reached by using `<hostname>.local`.

For an example: If you are connected to host `web` and you want to connect to the host `db` via ssh, you can use the following command:

    $ ssh db.local

(Side-note: vagrants standard password for the `vagrant` user is `vagrant`.)

## HTTP

Also there are two more forwarded ports defined in `./servers.json` which are 80 and 8080 on the virtual machine. So if a webservice is running on port 80 inside a virtual machine, for an example, the page can be visited in a browser on the physical host by using the url

    http://localhost:<port80_of_vm_n>

# Usage

## Start VMs

The following command installs the virtual machines and starts them:

    $ vagrant up

## Login to the VMs

    $ vagrant ssh <name_of_vm>
by using the name defined in ``./servers.json`. The same can be achived by connecting via native ssh as explained in [SSH](#SSH).

## Destroy the VMs after use

    $ vagrant destroy

## Note on multi-machine environment.

All vagrant commands can be used by specifying a single machine. Also some commands (as `vagrant ssh <name>`) need that name parameter inside a multi-machine environment as given.

# Docker

[Docker](https://www.docker.com/) is preinstalled in all virtual machines so the experiments with it can start right away.

The files of the [tutorial](https://docs.docker.com/get-started/) example is also already available inside the virtual machines on the path `/home/vagrant/docker`. That path is a shared folder with the physical host and all virtual machines, so changes inside that folder are visible on all machines. Although the images and other configurations are not yet build or started, because it should be a clean setup for your own experiments.