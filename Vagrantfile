# -*- mode: ruby -*-
# # vi: set ft=ruby :

Vagrant.require_version '>= 1.6.0'
VAGRANTFILE_API_VERSION = '2'

require 'json'
 
servers = JSON.parse(File.read(File.join(File.dirname(__FILE__), 'servers.json')))
 
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  servers.each do |server|
    config.vm.define server['name'] do |srv|
      srv.vm.box = server['box']
      srv.vm.hostname = server['name']
      srv.vm.network 'private_network', ip: server['ip_addr'],
        virtualbox__intnet: true
      srv.vm.network 'forwarded_port', guest: 22, host: server['ssh_port']
      srv.vm.network 'forwarded_port', guest: 80, host: server['port80']
      srv.vm.network 'forwarded_port', guest: 8080, host: server['port8080']
      srv.vm.synced_folder "./dockerfiles", "/home/vagrant/docker"
      srv.vm.provider "virtualbox" do |vb|
        vb.name = server['name']
        vb.memory = server['ram']
        vb.cpus = server['cpus']
      end
    end
  end

  config.vm.provision "shell", inline: <<-SHELL
    apt-get install -y avahi-daemon libnss-mdns
  SHELL

  config.vm.provision "docker"
end